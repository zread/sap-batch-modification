﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 31/01/2012
 * Time: 11:44 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Batch_Modification
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.CB_Wattage = new System.Windows.Forms.CheckBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.Cb_Po = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtChange = new System.Windows.Forms.Button();
            this.BtRevert = new System.Windows.Forms.Button();
            this.BtCheck = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(150, 274);
            this.textBox1.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Dark",
            "Blue",
            "Lite"});
            this.comboBox1.Location = new System.Drawing.Point(324, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(214, 12);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(62, 24);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Color";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // CB_Wattage
            // 
            this.CB_Wattage.Location = new System.Drawing.Point(214, 60);
            this.CB_Wattage.Name = "CB_Wattage";
            this.CB_Wattage.Size = new System.Drawing.Size(74, 24);
            this.CB_Wattage.TabIndex = 10;
            this.CB_Wattage.Text = "Wattage";
            this.CB_Wattage.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(324, 64);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            350,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(62, 20);
            this.numericUpDown1.TabIndex = 14;
            this.numericUpDown1.Value = new decimal(new int[] {
            310,
            0,
            0,
            0});
            // 
            // Cb_Po
            // 
            this.Cb_Po.Location = new System.Drawing.Point(214, 106);
            this.Cb_Po.Name = "Cb_Po";
            this.Cb_Po.Size = new System.Drawing.Size(97, 24);
            this.Cb_Po.TabIndex = 15;
            this.Cb_Po.Text = "PO";
            this.Cb_Po.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(324, 110);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(165, 20);
            this.textBox2.TabIndex = 16;
            // 
            // checkBox5
            // 
            this.checkBox5.Location = new System.Drawing.Point(214, 155);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(62, 24);
            this.checkBox5.TabIndex = 17;
            this.checkBox5.Text = "LID";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.Visible = false;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "ON",
            "OT",
            "XN"});
            this.comboBox3.Location = new System.Drawing.Point(324, 158);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(108, 21);
            this.comboBox3.TabIndex = 18;
            this.comboBox3.Visible = false;
            // 
            // checkBox3
            // 
            this.checkBox3.Location = new System.Drawing.Point(214, 204);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(97, 24);
            this.checkBox3.TabIndex = 19;
            this.checkBox3.Text = "Model Type";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "CS6P-P",
            "CS6P-M",
            "CS6X-P",
            "CS6X-M"});
            this.comboBox2.Location = new System.Drawing.Point(324, 207);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(108, 21);
            this.comboBox2.TabIndex = 20;
            this.comboBox2.Visible = false;
            // 
            // checkBox6
            // 
            this.checkBox6.Location = new System.Drawing.Point(214, 251);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(82, 24);
            this.checkBox6.TabIndex = 21;
            this.checkBox6.Text = "Cell Type";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.Visible = false;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "P",
            "M"});
            this.comboBox4.Location = new System.Drawing.Point(324, 254);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(108, 21);
            this.comboBox4.TabIndex = 22;
            this.comboBox4.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.PO,
            this.Status});
            this.dataGridView1.Location = new System.Drawing.Point(19, 292);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(699, 141);
            this.dataGridView1.TabIndex = 31;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Serial Number";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.Width = 120;
            // 
            // PO
            // 
            this.PO.HeaderText = "PO";
            this.PO.Name = "PO";
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            // 
            // BtChange
            // 
            this.BtChange.Location = new System.Drawing.Point(624, 140);
            this.BtChange.Name = "BtChange";
            this.BtChange.Size = new System.Drawing.Size(75, 23);
            this.BtChange.TabIndex = 32;
            this.BtChange.Text = "Modify";
            this.BtChange.UseVisualStyleBackColor = true;
            this.BtChange.Click += new System.EventHandler(this.BtChangeClick);
            // 
            // BtRevert
            // 
            this.BtRevert.Location = new System.Drawing.Point(624, 185);
            this.BtRevert.Name = "BtRevert";
            this.BtRevert.Size = new System.Drawing.Size(75, 23);
            this.BtRevert.TabIndex = 33;
            this.BtRevert.Text = "Wating List";
            this.BtRevert.UseVisualStyleBackColor = true;
            this.BtRevert.Click += new System.EventHandler(this.BtRevertClick);
            // 
            // BtCheck
            // 
            this.BtCheck.Location = new System.Drawing.Point(624, 235);
            this.BtCheck.Name = "BtCheck";
            this.BtCheck.Size = new System.Drawing.Size(75, 23);
            this.BtCheck.TabIndex = 34;
            this.BtCheck.Text = "Check Log";
            this.BtCheck.UseVisualStyleBackColor = true;
            this.BtCheck.Click += new System.EventHandler(this.BtCheckClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.BtCheck);
            this.Controls.Add(this.BtRevert);
            this.Controls.Add(this.BtChange);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.Cb_Po);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.CB_Wattage);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox1);
            this.Name = "MainForm";
            this.Text = "Batch Modification";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.DataGridViewTextBoxColumn PO;
		private System.Windows.Forms.Button BtCheck;
		private System.Windows.Forms.Button BtRevert;
		private System.Windows.Forms.DataGridViewTextBoxColumn Status;
		private System.Windows.Forms.Button BtChange;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.ComboBox comboBox4;
		private System.Windows.Forms.CheckBox checkBox6;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.CheckBox Cb_Po;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox CB_Wattage;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.TextBox textBox1;
		
		
	}
}
