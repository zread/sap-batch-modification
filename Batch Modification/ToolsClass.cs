﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/22/2016
 * Time: 9:24 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
//using System;
//using System.Data.SqlClient;
//using System.Xml;
//using System.Data;
//using System.Threading;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Data;
using System.Threading;

namespace Batch_Modification
{
	/// <summary>
	/// Description of ToolsClass.
	/// </summary>
	public class ToolsClass
	{
		public ToolsClass()
		{
		}
		
		public static string getSQLString()
		{
			string xmlFile = Application.StartupPath + "\\config.xml";
			XmlReader reader = XmlReader.Create(xmlFile);
			reader.ReadToFollowing("DataSource");
			string datasource = reader.ReadElementContentAsString();
			reader.ReadToFollowing("UserID");
			string userid = reader.ReadElementContentAsString();
			reader.ReadToFollowing("Password");
			string password = reader.ReadElementContentAsString();
			reader.ReadToFollowing("DBName");
			string dbname = reader.ReadElementContentAsString();
			return "Data Source= "+datasource+"; Initial Catalog= "+dbname+";User ID="+userid+";Password= "+password;
		}
		
		public static SqlDataReader GetDataReader(string SqlStr,string sDBConn)
        {
            SqlDataReader dr = null;       
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(sDBConn);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }
		
		public static DataTable SqlGetData (string Query, string connDB = "")
		{
			if (connDB == "") {
				connDB = getSQLString();
			}
			DataTable dt = new DataTable();
			SqlConnection conn = new SqlConnection(connDB);
            SqlCommand cmd = new SqlCommand (Query, conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            da.Dispose();
            return dt;
		}
	}
}
