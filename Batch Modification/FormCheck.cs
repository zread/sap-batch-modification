﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/29/2016
 * Time: 1:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Data;
using System.Threading;


namespace Batch_Modification
{
	/// <summary>
	/// Description of FormCheck.
	/// </summary>
	public partial class FormCheck : Form
	{
		public FormCheck(string userNM)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			username = userNM;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		private string username = "";
		
		 void ShowHistory(string DF, string DT, string SN)
		{
			
			
		 	DataTable dt = new DataTable();			
			string sql = "";
			string connDB = ToolsClass.getSQLString();	
		
			if ((!string.IsNullOrEmpty(SN))&&(checkbox_date.Checked)) {
				sql = @"select * From ModificationRecords where SN = '{0}' and Timestamp between '{1}' and '{2}' order by timestamp desc";
           		sql = string.Format(sql,SN,DF,DT);
			}
			else if((!string.IsNullOrEmpty(SN))&&(checkbox_date.Checked == false))
			{
				sql = @"select * From ModificationRecords where SN = '{0}' order by timestamp desc";
				sql = string.Format(sql,SN);
			}
			else if((string.IsNullOrEmpty(SN))&&(checkbox_date.Checked)){
				sql = @"select * From ModificationRecords where Timestamp between '{0}' and '{1}' order by timestamp desc";
	            sql = string.Format(sql,DF,DT);
			}
			else
			{
				MessageBox.Show("Please enter at least one condition!");
				return;
			}
            
            SqlConnection conn = new SqlConnection(connDB);
            SqlCommand cmd = new SqlCommand (sql, conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            da.Dispose();


            dataGridView1.DataSource = dt;         	  
		}
			
		    
		void U_HistoryClick(object sender, System.EventArgs e)
		{
			string datefrom = "";
			string dateto = "";
			string sn = "";
			if (checkbox_date.Checked) {
				datefrom = dateTimePicker1.Value.ToString();
				dateto = dateTimePicker2.Value.ToString();
			}
			if(!string.IsNullOrEmpty(TB_SN.Text.ToString()))
				sn = TB_SN.Text.ToString();
			ShowHistory(datefrom,dateto,sn);
		}	
		
		void Checkbox_dateCheckedChanged(object sender, System.EventArgs e)
		{
			if(checkbox_date.Checked == false)
			{
				dateTimePicker1.Enabled = false;
				dateTimePicker2.Enabled = false;
				
			}
			else
			{
				dateTimePicker1.Enabled = true;
				dateTimePicker2.Enabled = true;
			}
		}
			
	}
}
