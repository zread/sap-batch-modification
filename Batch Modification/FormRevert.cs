﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 1/19/2016
 * Time: 2:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Data;
using System.Threading;


namespace Batch_Modification
{
	
	/// <summary>
	/// Description of FormRevert.
	/// </summary>
	public partial class FormRevert : Form
	{
		int change = 0;
		public string username = "";
		private SqlDataReader reader = null;
		public FormRevert(string User)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			this.username = User;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			username = User;
		}
			
			//DataTable reader = new DataTable();
			
		
		void U_HistoryClick(object sender, EventArgs e)
		{
			 ShowHistory();
		}
		
		
		
		void BtApproveClick(object sender, EventArgs e)
		{
				
			if(dataGridView1.Rows.Count > 0)
			{
				for(int i = 0; i < dataGridView1.Rows.Count ; i ++)
				{
					//GET ALL SELECTED ROWS
					DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dataGridView1.Rows[i].Cells[0];
					
					if( (bool)chk.Value == true)
					{
						#region initializing Variables	
						List<string> Category = new List<string>();						
						ChangeList temp = new ChangeList();						
					
						temp.SN = dataGridView1.Rows[i].Cells["Column1"].Value.ToString();
		          	  	 	
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["Timestamp"].Value.ToString()))
		          	  		temp.Timestamp = dataGridView1.Rows[i].Cells["Timestamp"].Value.ToString();	          	  		
		          	  	
		          	    
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["FromWattage"].Value.ToString()))
		          	  	{
							temp.FromWattage = dataGridView1.Rows[i].Cells["FromWattage"].Value.ToString();
		          	  		Category.Add("Wattage");
		          	  	}
						if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["ToWattage"].Value.ToString()))
							temp.ToWattage = dataGridView1.Rows[i].Cells["ToWattage"].Value.ToString();					   
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["FromPO"].Value.ToString()))
		          	  	{
							temp.FromPO = dataGridView1.Rows[i].Cells["FromPO"].Value.ToString();
							Category.Add("PO");
		          	  	}
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["ToPO"].Value.ToString()))
		          	  		temp.ToPO = dataGridView1.Rows[i].Cells["ToPO"].Value.ToString();
		          	  	
		          	  	temp.UserName = dataGridView1.Rows[i].Cells["User"].Value.ToString();
						#endregion
						
						
								for (int j = 0; j < Category.Count; j++)
							{
								switch(Category[j])
								{
									case "Color":
										Modify_Color(temp);
										break;
									case "Wattage":
										Modify_Wattage(temp);
										break;												
									case "PO":
										Modify_PO(temp);
										break;	
								 
			  
								} 
							
							}
							if(change == 1)
						   	{
							    //log changes
							    string connDB = ToolsClass.getSQLString();	
							    using (SqlConnection con = new SqlConnection(connDB))
								{
							    	con.Open();
							    	using (SqlCommand command = new SqlCommand("update ModificationRecords set ApprovedBy = '"+ username + "' where username = '"+ temp.UserName+ "' and SN = '" + temp.SN + "' and Frompo = '" + temp.FromPO +"' and ToPO = '"+temp.ToPO + "'", con))
					            	{
					            		command.ExecuteNonQuery();	            		       		
					            	}
							    	con.Close();
							    }
							    
							  
							    
						    }
						    else
						    {
						    	
						    }
						change = 0;	
						string connstring = ToolsClass.getSQLString();	
					}
				}
				
				  ShowHistory();
			}
			
		}
		
	
		void Modify_Color(ChangeList T)
		{
			string connDB = ToolsClass.getSQLString();	
				    using (SqlConnection con = new SqlConnection(connDB))
					{
				    	  con.Open();
						using (SqlCommand command = new SqlCommand("update product set cellcolor = '"+ T.ToColor +"' where prodid = (select max(prodid) from product where sn = '"+ T.SN +"')", con))
		                	{
		                		command.ExecuteNonQuery();
		                	}
						con.Close();
				    }
				    change = 1;
				   
		}
		void Modify_Wattage(ChangeList T)
		{
			string connDB = ToolsClass.getSQLString();	
			using (SqlConnection con = new SqlConnection(connDB))
			{
				  con.Open();
                string sql = "";
                if (T.ToWattage == "0")
                {
                    sql = "delete from ModuleDownGrade where SN = '{0}'";
                    sql = string.Format(sql, T.SN);
                }
                else
                {
                    sql = "insert into ModuleDownGrade values ('{0}','{1}','{2}',getdate())";
                    sql = string.Format(sql, T.SN, T.ToWattage, T.UserName);
                }
                


                using (SqlCommand command = new SqlCommand(sql, con))
	                	{
	                		command.ExecuteNonQuery();
	                	}
				    
				    	con.Close();
			}
			    	
			    	change = 1;
		}
		void Modify_MaterialCode(ChangeList T)
		{
			bool update = false;
			string connDB = ToolsClass.getSQLString();	
			string Material_B = "";
			using (SqlConnection con = new SqlConnection(connDB))
			{
				con.Open();
			    	string materialcode = T.ToMaterialCode;
			    	using (SqlCommand command = new SqlCommand("Select [MaterialCode] from Product where prodid = (select max(prodid) from product where SN = '"+T.SN+"')", con))
		        	using (reader = command.ExecuteReader())
		        	{
			    		if(reader.Read())			    	
		        		{
			    			Material_B = reader.GetString(0);
			    		}			    		
			    	}
			    	using (SqlCommand command = new SqlCommand("select [NewTempTable].[NominalPower] from newtemptable where elecid = (select max(elecid) from newtemptable where fnumber = '"+T.SN+"')", con))
		        	using (reader = command.ExecuteReader())
		        	{
		        		if(reader.Read())
		        		{
		        			update = true;
		        			string np = reader.GetInt32(0).ToString();
		        			materialcode = materialcode.Substring(0, 16) + np + Material_B.Substring(19, 1);
		        		}
		        	}

			    	if(update == true)
			    	{
			    		using (SqlCommand command = new SqlCommand("update product set materialcode = '"+materialcode+"' where prodid = (select max(prodid) from product where SN = '"+T.SN+"')", con))
			    		{
		                	command.ExecuteNonQuery();
			    		}
			    		//T.ToMaterialCode = materialcode;		    		
			    	}
			    	con.Close();
			}
			    	change = 1;
		}
		
		
		void Modify_PO(ChangeList T)
		{
			string connDB = ToolsClass.getSQLString();
			using (SqlConnection con = new SqlConnection(connDB))
			{
				  con.Open();
				  using (SqlCommand command = new SqlCommand("insert into po_modification values('"+T.SN+"','"+T.ToPO+"')", con))
				  {
				  	command.ExecuteNonQuery();
				  }
				  con.Close();
			}
			change = 1;
		}
	
		
			 
		void ShowHistory()
		{
			
			DataTable dt = new DataTable();
			dataGridView1.Rows.Clear();
			string connDB = ToolsClass.getSQLString();	
		
			string sql = @"select [Username]
							      ,[SN]
							      ,[Timestamp]
							      ,[FromWattage]
							      ,[ToWattage]
							      ,[FromPO]
							      ,[ToPO] From ModificationRecords where ApprovedBy = '' order by timestamp desc";
          
            
            
            SqlConnection conn = new SqlConnection(connDB);
            SqlCommand cmd = new SqlCommand (sql, conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            da.Dispose();
				
		    				    		
          	  foreach (DataRow row in dt.Rows)
	        {
          	string _UserName = "";
          	string _SN = "";
			string _Timestamp = "";		
			string _FromWattage = "";
			string _ToWattage = "";			
			string _FromPO ="";
			string _ToPO ="";
          	 
          	  	if(!string.IsNullOrEmpty(row["SN"].ToString()))
          	  	 	_SN = row["SN"].ToString();
          	  	if(!string.IsNullOrEmpty(row["Timestamp"].ToString()))
          	  		_Timestamp = row["Timestamp"].ToString(); 	      
          	  	if(!string.IsNullOrEmpty(row["FromWattage"].ToString()))
					_FromWattage = row["FromWattage"].ToString();
          	  	if(!string.IsNullOrEmpty(row["ToWattage"].ToString()))
					_ToWattage = row["ToWattage"].ToString();					   
				if(!string.IsNullOrEmpty(row["FromPO"].ToString()))
					_FromPO = row["FromPO"].ToString();
          	  	if(!string.IsNullOrEmpty(row["ToPO"].ToString()))
					_ToPO = row["ToPO"].ToString();	
          	  	if(!string.IsNullOrEmpty(row["Username"].ToString()))
          	  		_UserName = row["Username"].ToString();         
          	  	dataGridView1.Rows.Insert(dataGridView1.RowCount, new object[] {false,_SN,_Timestamp,_FromWattage,_ToWattage,_FromPO,_ToPO,_UserName});
		    }          	  
		}
			
		    	
			
		
		
		
		void BtRejectClick(object sender, EventArgs e)
		{
			if(dataGridView1.Rows.Count < 1)
				return;
			for(int i = 0; i < dataGridView1.Rows.Count ; i ++)
				{
					//GET ALL SELECTED ROWS
					DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dataGridView1.Rows[i].Cells[0];
					
					if( (bool)chk.Value == true)
					{
						#region initializing Variables					
						ChangeList temp = new ChangeList();						
						//if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["Column1"].ToString()))
						temp.SN = dataGridView1.Rows[i].Cells["Column1"].Value.ToString();
		          	  	 	
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["Timestamp"].Value.ToString()))
		          	  		temp.Timestamp = dataGridView1.Rows[i].Cells["Timestamp"].Value.ToString();	          	  		
		          	  	
		          	    
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["FromWattage"].Value.ToString()))
		          	  	{
							temp.FromWattage = dataGridView1.Rows[i].Cells["FromWattage"].Value.ToString();
		          	  		
		          	  	}
						if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["ToWattage"].Value.ToString()))
							temp.ToWattage = dataGridView1.Rows[i].Cells["ToWattage"].Value.ToString();					   
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["FromPO"].Value.ToString()))
		          	  	{
							temp.FromPO = dataGridView1.Rows[i].Cells["FromPO"].Value.ToString();
							
		          	  	}
		          	  	if(!string.IsNullOrEmpty(dataGridView1.Rows[i].Cells["ToPO"].Value.ToString()))
		          	  		temp.ToPO = dataGridView1.Rows[i].Cells["ToPO"].Value.ToString();
		          	  	
		          	  	temp.UserName = dataGridView1.Rows[i].Cells["User"].Value.ToString();
						#endregion
						
					string connDB = ToolsClass.getSQLString();	
					string sql = @"Delete From Modificationrecords where  sn = '{0}' and fromwattage = '{1}' and towattage = '{2}' and fromPO = '{3}' and toPO = '{4}' and Username = '{5}' ";
					sql = string.Format(sql,temp.SN,temp.FromWattage,temp.ToWattage,temp.FromPO,temp.ToPO,temp.UserName);
					using (SqlConnection con = new SqlConnection(connDB))
					{
				    	con.Open();
				    	using (SqlCommand command = new SqlCommand(sql, con))
		            	{
		            		command.ExecuteNonQuery();	            		       		
		            	}
				    	con.Close();
				    }
					}
				
				}
				
					ShowHistory();
		}
			
		
		void BtselectClick(object sender, EventArgs e)
		{
			foreach (DataGridViewRow row in dataGridView1.Rows) {
				DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
					if( (bool)chk.Value == true)
						chk.Value = false;
					else 
						chk.Value = true;
			}
		}
	}
	
}
		
		
			    
