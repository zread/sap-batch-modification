﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 31/01/2012
 * Time: 11:44 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Data;
using System.Threading;

namespace Batch_Modification
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm(string user, string pw)
		{
			InitializeComponent();
			this.username = user;
			this.password = pw;
		}
		
		private string username;
		private string password;
	
		
		void modify(string sn)
		{
		    int change = 0;
		    string connDB = ToolsClass.getSQLString();			
			
			SqlDataReader reader = ToolsClass.GetDataReader("select sn, StdPower from product where InterID = (select max(InterID) from product where sn = '"+sn+"')", connDB);
			while(reader.Read())
			{
				dataGridView1.Rows.Insert(dataGridView1.RowCount-1, new object[] { reader.GetValue(0), reader.GetValue(1)});				
			}
			
			using (SqlConnection con = new SqlConnection(connDB))
			{
				string fromColor = "";
				string fromWattage = "";
				string fromMaterialCode = "";
				string fromLID = "";
				string toColor = "";
				string toWattage = "";
				string toMaterialCode = "";
				string toLID = "";
				string frommodelType = "";
				string fromcellType = "";
				string tomodelType = "";
				string tocellType = "";
				string FromPO = sn.Substring(7,3);
				string ToPO = "";
				
				//read original properties
			    con.Open();
		    	using (SqlCommand command = new SqlCommand("select sn, StdPower from product where InterID = (select max(InterID) from product where sn = '"+sn+"')", con))
	        	using (reader = command.ExecuteReader())
	        	{			    		
		    		if(reader.Read() && reader != null)
	        		{
                        
		    			if(reader.IsDBNull(1) == false)
		    				fromWattage = reader.GetString(1);
		    		}
		    		else
		    		{
		    			MessageBox.Show("Modifying "+sn+" fails, this SN does not exist in packaging system database.");
		    			return;
		    		}
	        	}
			    
		    	//modify color
			    if(checkBox1.Checked == true)
			    {
//				    using (SqlCommand command = new SqlCommand("update product set cellcolor = '"+comboBox1.Text+"' where prodid = (select max(prodid) from product where sn = '"+sn+"')", con))
//                	{
//                		command.ExecuteNonQuery();
//                	}
				    toColor = comboBox1.Text;
				    change = 1;
			    }
			    else
			    {
			    	fromColor = "";
			    }
			    
			    //modify power
			    if(CB_Wattage.Checked == true)
			    {
			    	toWattage = numericUpDown1.Value.ToString();
			    	
			    	change = 1;
			    }
			    else
			    {
			    	fromWattage = "";
			    }


			    //modify PO
			    if(Cb_Po.Checked == true)
			    {
			    	
			    	ToPO =  textBox2.Text.ToString();
			    	
			    	change = 1;
			    }
			    else
			    {
			    	FromPO = "";
			    }
			    
			    //modify LID
			    if(checkBox5.Checked == true)
			    {
//			    	using (SqlCommand command = new SqlCommand("update product set lid = '"+comboBox3.Text+"' where prodid = ( select max(prodid) from product where sn = '"+sn+"')", con))
//                	{
//                		command.ExecuteNonQuery();
//                	}
			    	toLID = comboBox3.Text;
			    	change = 1;
			    }
			    else
			    {
			    	fromLID = "";
			    }
			    
			    // Modify Model Type
			    if(checkBox3.Checked == true)
			    {
//			    	using (SqlCommand command = new SqlCommand("update product set ModelType = '"+comboBox2.Text+"' where prodid = ( select max(prodid) from product where sn = '"+sn+"')", con))
//                	{
//                		command.ExecuteNonQuery();
//                	}
			    	tomodelType = comboBox2.Text;
			    	change = 1;
			    }
			    else
			    {
			    	frommodelType = "";
			    }
			    
			    // Modify Cell Type
			    if(checkBox6.Checked == true)
			    {
//			    	using (SqlCommand command = new SqlCommand("update product set CellType = '"+comboBox4.Text+"' where prodid = ( select max(prodid) from product where sn = '"+sn+"')", con))
//                	{
//                		command.ExecuteNonQuery();
//                	}
			    	tocellType = comboBox4.Text;
			    	change = 1;
			    }
			    else
			    {
			    	fromcellType = "";
			    }
			    
			    if(change == 1){
				    //log changes
			    	using (SqlCommand command = new SqlCommand("insert into ModificationRecords ([Username],[SN],[Timestamp],[FromWattage],[ToWattage],[FromPO] ,[ToPO] ,[ApprovedBy]) values ('"+username+"', '"+sn+"',getdate()" +",'"+fromWattage+"','"+toWattage+"','"+FromPO+"','"+ToPO+"','')", con))
	            	{
	            		command.ExecuteNonQuery();	            		       		
	            	}
				    dataGridView1.Rows[dataGridView1.RowCount-2].Cells["Status"].Value = "Succeed";
			    }
			    else
			    	dataGridView1.Rows[dataGridView1.RowCount-2].Cells["Status"].Value = "Unchanged";
			    
			}
		}
	
		
		void BtChangeClick(object sender, EventArgs e)
		{
			if(!string.IsNullOrEmpty(textBox1.Text.ToString()))
			{
                string strTemp = textBox1.Text;
                string[] sDataSet = strTemp.Split('\n');
                dataGridView1.Rows.Clear();
                for (int i = 0; i < sDataSet.Length; i++)
                {
                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    if (sDataSet[i] != null && sDataSet[i] != "")
                    {
                    	modify(sDataSet[i]);
                    }
                }
                textBox1.Clear();
                
                checkBox1.Checked = false;
				CB_Wattage.Checked = false;
				checkBox3.Checked = false;
				Cb_Po.Checked = false;
				checkBox5.Checked = false;				
				checkBox6.Checked = false;
			}
			else
			{
				MessageBox.Show("Please Enter Serial Numbers!");
			}
		}
		
		void BtRevertClick(object sender, EventArgs e)
		{
			string sql = @"Select * from UserLogin where (UserGroup = '9' or UserGroup = '99') and UserNm = '{0}' and UserPW = '{1}' ";
			sql = string.Format(sql,username,password);
			string connDB = ToolsClass.getSQLString();
					
			if(ToolsClass.SqlGetData(sql).Rows.Count > 0 )
			{
			System.Threading.Thread.CurrentThread.ApartmentState = System.Threading.ApartmentState.STA;
			Form RV = new FormRevert(username);
			RV.ShowDialog();
			}
			else
			{
				MessageBox.Show("You Are Not Permitted!");
			}
		}
		
		void BtCheckClick(object sender, EventArgs e)
		{
			System.Threading.Thread.CurrentThread.ApartmentState = System.Threading.ApartmentState.STA;
			Form RV = new FormCheck(username);
			RV.ShowDialog();
		}
	}
}
