﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 1/19/2016
 * Time: 2:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Batch_Modification
{
	partial class FormRevert
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Bt_Approve = new System.Windows.Forms.Button();
			this.U_History = new System.Windows.Forms.Button();
			this.BtReject = new System.Windows.Forms.Button();
			this.Btselect = new System.Windows.Forms.Button();
			this.CheckBox1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FromWattage = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ToWattage = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FromPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ToPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
									this.CheckBox1,
									this.Column1,
									this.Timestamp,
									this.FromWattage,
									this.ToWattage,
									this.FromPO,
									this.ToPO,
									this.User});
			this.dataGridView1.Location = new System.Drawing.Point(-1, -3);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(1102, 445);
			this.dataGridView1.TabIndex = 32;
			this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
			// 
			// Bt_Approve
			// 
			this.Bt_Approve.Location = new System.Drawing.Point(488, 467);
			this.Bt_Approve.Name = "Bt_Approve";
			this.Bt_Approve.Size = new System.Drawing.Size(75, 23);
			this.Bt_Approve.TabIndex = 34;
			this.Bt_Approve.Text = "Approve";
			this.Bt_Approve.UseVisualStyleBackColor = true;
			this.Bt_Approve.Click += new System.EventHandler(this.BtApproveClick);
			// 
			// U_History
			// 
			this.U_History.Location = new System.Drawing.Point(235, 467);
			this.U_History.Name = "U_History";
			this.U_History.Size = new System.Drawing.Size(79, 23);
			this.U_History.TabIndex = 35;
			this.U_History.Text = "Show List";
			this.U_History.UseVisualStyleBackColor = true;
			this.U_History.Click += new System.EventHandler(this.U_HistoryClick);
			// 
			// BtReject
			// 
			this.BtReject.Location = new System.Drawing.Point(691, 467);
			this.BtReject.Name = "BtReject";
			this.BtReject.Size = new System.Drawing.Size(75, 23);
			this.BtReject.TabIndex = 36;
			this.BtReject.Text = "Reject";
			this.BtReject.UseVisualStyleBackColor = true;
			this.BtReject.Click += new System.EventHandler(this.BtRejectClick);
			// 
			// Btselect
			// 
			this.Btselect.Location = new System.Drawing.Point(369, 467);
			this.Btselect.Name = "Btselect";
			this.Btselect.Size = new System.Drawing.Size(79, 23);
			this.Btselect.TabIndex = 37;
			this.Btselect.Text = "Select All";
			this.Btselect.UseVisualStyleBackColor = true;
			this.Btselect.Click += new System.EventHandler(this.BtselectClick);
			// 
			// CheckBox1
			// 
			this.CheckBox1.HeaderText = "select";
			this.CheckBox1.Name = "CheckBox1";
			this.CheckBox1.Width = 40;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Serial Number";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 120;
			// 
			// Timestamp
			// 
			this.Timestamp.HeaderText = "Timestamp";
			this.Timestamp.Name = "Timestamp";
			this.Timestamp.ReadOnly = true;
			this.Timestamp.Width = 200;
			// 
			// FromWattage
			// 
			this.FromWattage.HeaderText = "FromWattage";
			this.FromWattage.Name = "FromWattage";
			this.FromWattage.ReadOnly = true;
			// 
			// ToWattage
			// 
			this.ToWattage.HeaderText = "ToWattage";
			this.ToWattage.Name = "ToWattage";
			this.ToWattage.ReadOnly = true;
			// 
			// FromPO
			// 
			this.FromPO.HeaderText = "FromPO";
			this.FromPO.Name = "FromPO";
			// 
			// ToPO
			// 
			this.ToPO.HeaderText = "ToPO";
			this.ToPO.Name = "ToPO";
			// 
			// User
			// 
			this.User.HeaderText = "User";
			this.User.Name = "User";
			// 
			// FormRevert
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(1101, 567);
			this.Controls.Add(this.Btselect);
			this.Controls.Add(this.BtReject);
			this.Controls.Add(this.U_History);
			this.Controls.Add(this.Bt_Approve);
			this.Controls.Add(this.dataGridView1);
			this.Name = "FormRevert";
			this.Text = "Wait List";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.DataGridViewTextBoxColumn User;
		private System.Windows.Forms.DataGridViewTextBoxColumn ToPO;
		private System.Windows.Forms.DataGridViewTextBoxColumn FromPO;
		private System.Windows.Forms.Button Btselect;
		private System.Windows.Forms.Button BtReject;
		private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox1;
		private System.Windows.Forms.Button U_History;
		private System.Windows.Forms.Button Bt_Approve;
	
		private System.Windows.Forms.DataGridViewTextBoxColumn ToWattage;
		private System.Windows.Forms.DataGridViewTextBoxColumn FromWattage;
		private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridView dataGridView1;
		
		void DataGridView1CellContentClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
		{
			
		}
	}
}
