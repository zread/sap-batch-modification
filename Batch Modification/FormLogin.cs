﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 7/24/2012
 * Time: 3:29 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;

namespace Batch_Modification
{
	/// <summary>
	/// Description of FormLogin.
	/// </summary>
	public partial class Login : Form
	{
		public Login()
		{
			InitializeComponent();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			string connectionString = ToolsClass.getSQLString();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand("select * from userlogin where (usergroup = '10' or usergroup = '9' or usergroup = '13' or usergroup = '99') and userNM = '"+textBox1.Text+"' and userPW = '"+textBox2.Text+"'", con))
			    using (SqlDataReader reader = command.ExecuteReader())
				{
		    		if(reader.Read() && reader != null)
		    		{
						this.Hide();
						System.Threading.Thread.CurrentThread.ApartmentState = System.Threading.ApartmentState.STA;
		    			Form mf = new MainForm(textBox1.Text,textBox2.Text);
						mf.ShowDialog();
						this.Close();	
						
		    		}
		    		else{
		    			MessageBox.Show("Sorry this user doesn't exist or doesn't have access.");
		    			textBox1.Clear();
		    			textBox2.Clear();
		    		}
				}
			}
		}
		
		void TextBox2TextChanged(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == '\r')
				button1.Focus();
		}
	}
}
