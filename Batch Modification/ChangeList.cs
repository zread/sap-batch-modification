﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/19/2016
 * Time: 4:47 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Batch_Modification
{
	/// <summary>
	/// Description of ChangeList.
	/// </summary>
	public partial class ChangeList : Form
	{
		public ChangeList()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
		
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
			private string _SN = "";
			private string _Timestamp = "";
			private string _FromColor = "";
			private string _ToColor = "";
			private string _FromWattage = "";
			private string _ToWattage = "";
			private string _FromMaterialCode = "";
			private string _ToMaterialCode = "";
			private string _FromLID = "";
			private string _ToLID = "";
			private string _FromModelType = "";
			private string _ToModelType = "";
			private string _FromCellType = "";
			private string _ToCellType = "";
			private string _FromPO = "";
			private string _ToPO = "";
			private string _UserName = "";
		
		
        public string SN
        {
            get { return _SN; }
            set { _SN= value; }
        }
        
        public string Timestamp
        {
        	get { return _Timestamp; }
        	set { _Timestamp  = value; }
        }
        
        public string FromColor
        {
        	get { return _FromColor; }
        	set { _FromColor = value; }
        }
        
        public string ToColor
        {
        	get { return _ToColor; }
        	set { _ToColor = value; }
        }
        
        public string FromWattage
        {
        	get { return _FromWattage; }
        	set { _FromWattage = value; }
        }
        
        public string ToWattage
        {
        	get { return _ToWattage; }
        	set {_ToWattage = value; }
        }
        
        public string FromMaterialCode
        {
        	get { return _FromMaterialCode; }
        	set { _FromMaterialCode = value; }
        	
        }
        
		 public string ToMaterialCode
        {
        	get { return _ToMaterialCode; }
        	set { _ToMaterialCode = value; }        	
        }
		 public string FromLID
        {
        	get { return _FromLID; }
        	set { _FromLID = value; }        	
        }
		 public string ToLID
        {
        	get { return _ToLID; }
        	set { _ToLID = value; }        	
        }
		 public string  FromModelType
        {
        	get { return  _FromModelType; }
        	set { _FromModelType = value; }        	
        }
		 public string ToModelType
        {
        	get { return _ToModelType; }
        	set { _ToModelType = value; }        	
        }
		  public string FromCellType
        {
        	get { return _FromCellType; }
        	set { _FromCellType = value; }        	
        } 
		  public string ToCellType
        {
        	get { return _ToCellType; }
        	set { _ToCellType = value; }        	
        }  

		   public string FromPO
        {
		   	get { return _FromPO; }
        	set { _FromPO = value; }        	
        }  
		  public string ToPO
        {
        	get { return _ToPO; }
        	set { _ToPO = value; }        	
        } 
		  public string UserName
        {
        	get { return _UserName; }
        	set { _UserName = value; }        	
        }  			  
	}
}
