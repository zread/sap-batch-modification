﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 1/19/2016
 * Time: 2:26 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Batch_Modification
{
	partial class FormCheck
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.U_History = new System.Windows.Forms.Button();
            this.TB_SN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkbox_date = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(-1, -3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(733, 445);
            this.dataGridView1.TabIndex = 32;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
            // 
            // U_History
            // 
            this.U_History.Location = new System.Drawing.Point(609, 515);
            this.U_History.Name = "U_History";
            this.U_History.Size = new System.Drawing.Size(79, 23);
            this.U_History.TabIndex = 35;
            this.U_History.Text = "ShowHistroy";
            this.U_History.UseVisualStyleBackColor = true;
            this.U_History.Click += new System.EventHandler(this.U_HistoryClick);
            // 
            // TB_SN
            // 
            this.TB_SN.Location = new System.Drawing.Point(104, 473);
            this.TB_SN.Name = "TB_SN";
            this.TB_SN.Size = new System.Drawing.Size(149, 20);
            this.TB_SN.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(33, 473);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 20);
            this.label1.TabIndex = 37;
            this.label1.Text = "SN";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(115, 520);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 38;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(390, 520);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(71, 521);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 40;
            this.label2.Text = "From:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(346, 521);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 20);
            this.label3.TabIndex = 41;
            this.label3.Text = "To:";
            // 
            // checkbox_date
            // 
            this.checkbox_date.Location = new System.Drawing.Point(32, 516);
            this.checkbox_date.Name = "checkbox_date";
            this.checkbox_date.Size = new System.Drawing.Size(23, 24);
            this.checkbox_date.TabIndex = 42;
            this.checkbox_date.UseVisualStyleBackColor = true;
            this.checkbox_date.CheckedChanged += new System.EventHandler(this.Checkbox_dateCheckedChanged);
            // 
            // FormCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(730, 567);
            this.Controls.Add(this.checkbox_date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_SN);
            this.Controls.Add(this.U_History);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormCheck";
            this.Text = "FormCheck";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private System.Windows.Forms.CheckBox checkbox_date;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dateTimePicker2;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox TB_SN;
		private System.Windows.Forms.Button U_History;
		private System.Windows.Forms.DataGridView dataGridView1;
		
		void DataGridView1CellContentClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
		{
			
		}
		
		
		
	}
}
